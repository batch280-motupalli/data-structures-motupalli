import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

#1
plt.figure(figsize=(10, 5))

x = [2008, 2009, 2010, 2011, 2012]
y = [6, 12, 10, 14, 18]

plt.plot(x, y,'.-',color = "turquoise", label = "movies watched", markersize=20, markeredgecolor='blue')
plt.legend()

plt.title("Movies watched by kim's family 2008-2012")
plt.xlabel("Year")
plt.ylabel("Number of Movies")

plt.xticks([2008, 2009, 2010, 2011, 2012])
plt.yticks([6, 8, 10, 12, 14, 16, 18, 20])

plt.savefig("activity/line_chart1.png")

plt.show()

#2
plt.figure(figsize=(10, 5))

x = [2008, 2009, 2010, 2011, 2012, 2013]
y1 = [700, 500, 550, 645, 300, 200]
y2 = [650, 700, 480, 800, 200, 300]

plt.plot(x, y1,'.-',color = "gold", label = "Island Middle School", markersize=20, markeredgecolor='yellow')
plt.plot(x, y2,'.--',color = "dodgerblue", label = "Eden Middle School", markersize=20, markeredgecolor='blue')
plt.legend()

plt.title("Island vs Eden collection of Aluminium Beverage Cans")
plt.xlabel("Year")
plt.ylabel("Weight of Cans in Pounds")

plt.xticks([2008, 2009, 2010, 2011, 2012, 2013])
plt.yticks([200, 300, 400, 500, 600, 700, 800, 900])

plt.savefig("activity/line_chart2.png")

plt.show()

#3
plt.figure(figsize=(10, 5))

insects = ['Ant', 'Lady Bug', 'Grasshopper', 'Spider']
counts = [3, 10, 5, 8]
bar_colors = ['black', 'tomato', 'seagreen', 'darkgrey']

plt.barh(y = insects,width = counts, color= bar_colors)

plt.xlabel("Total Count")
plt.ylabel("Insects Type")
plt.title("jason's Insects")

plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

plt.savefig("activity/bar_graph.png")
plt.show()

#4
plt.figure(figsize = (10, 5))

d1 = [46, 54, 33, 68, 43, 39, 60]
d2 = [58, 67, 44, 72, 51, 42, 60, 46, 69]
d3 = [67, 100, 94, 77, 80, 62, 79, 68, 95, 86, 73, 84]

plt.boxplot([d1, d2, d3])

plt.title('Box and Whisker Plot for Three Data Sets')
plt.xlabel('Data Sets')
plt.ylabel('Values')

plt.savefig("activity/boxplot.png")
plt.show()

#5
plt.figure(figsize = (10, 5))

labels = ['Collecting seashells', 'Volleyball', 'Sandcastle building', 'Kite flying', 'Surfing']
p = [17, 15,21, 17, 30]
explode = [0, 0, 0.1, 0, 0.1]

plt.pie(p, labels = labels, autopct='%1.1f%%', startangle=140, explode = explode)

plt.title("Mrs. Carolyn's Class favorite Beach Activities")

plt.savefig("activity/Pie chart.png")
plt.show()