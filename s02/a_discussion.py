# [SECTION] Creating and Traversing through Binary Tree

class BinaryTree:
	def __init__(self, data):
		self.data = data
		self.left = None 
		self.right = None 
		self.size = 0 

	# Allows us to navigate the tree in this order:
	# ROOT -> LEFT NODE -> RIGHT NODE
	def preorder_print(self, root):
		if root is not None: #If the root has data
			self.size += 1
			print(root.data, end=" -> ")

			# Using recursion we are able to utilize the values of the left and right node to add size to the tree and print the values of each node
			self.preorder_print(root.left)
			self.preorder_print(root.right)

	def get_size(self):
		if self.size == 0:
			return 'The size of the tree is 0'

		return 'The size of the tree is {size}'.format(size = self.size)

	def is_empty(self):
		if self.size == 0:
			return True 
		else: 
			return False 

	def height(self, root):
		if root == None:
			self.size == 0
			return 0 

		if self.is_empty():
			return max(self.height(root.left), self.height(root.right))

		return max(self.height(root.left), self.height(root.right)) + 1

	# Allows us to navigate the tree in this order:
	# LEFT NODE -> ROOT -> RIGHT NODE
	def inorder_print(self, root):
		if root is not None:
			self.size += 1
			self.inorder_print(root.left)
			print(root.data, end=" -> ")
			self.inorder_print(root.right)

	# Allows us to navigate the tree is this order:
	# LEFT NODE -> RIGHT NODE -> ROOT
	def postorder_print(self, root):
		if root is not None:
			self.size += 1 
			self.postorder_print(root.left)
			self.postorder_print(root.right)
			print(root.data, end=" -> ")


root = BinaryTree('President')
root.left = BinaryTree('VP for internal')
root.right = BinaryTree('VP for external')

root.preorder_print(root)
print('')
print(root.get_size())
print('Is empty? ', root.is_empty())
print('Height: ', root.height(root))
print('--------')

root.inorder_print(root)
print('')
print('-------')
root.postorder_print(root)