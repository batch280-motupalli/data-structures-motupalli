import binarytree as bt 
from binarytree import build 
from binarytree import bst

nodes = [3, 2, 2, None,5 ,5, None]
root = build(nodes)

print(root)
print(f'Is this a balanced tree? {root.is_balanced}')
print(f'Is this a symmetrical tree? {root.is_symmetric}')
print(f'Height of the balanced tree: {root.height}')

root = bst(2)

print(root)
print(f'Is this a BST? {root.is_bst}')
print(f'Is this BST strict {root.is_strict}')
print(f'The min value is: {root.min_node_value}')
print(f'The max value is: {root.max_node_value}')

root = build([1, 2, 3, 4, 5])
print(root)
print(f'Is this a complete tree? {root.is_complete}')
print(f'The leaf count is: {root.leaf_count}')
print(f'The second level elements are: {root.levels[1]}')
